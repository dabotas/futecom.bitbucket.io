/**
 * Base de Dados
 */

var jogadores = [];
var equipas = [];
var ligas = [];
var tacas = [];
var paises = ['Alemanha', 'Itália', 'Portugal', 'Espanha', 'Inglaterra', 'França', 'Holanda', 'Bélgica', 'Polónia', 'Dinamarca', 'Madeira', 'Brasil'];
var posicoes = ["Guarda Redes", "Defesa", "Médio", "Avançado", ]

/**
 * @function iniciarBD Iniciar aplicação com valores predefinidos
 */
function iniciarBD() {
    var j1 = {
        nome: "Cristiano Ronaldo",
        dataNascimento: new Date("1980-1-1"),
        altura: "189",
        pais: 'Madeira',
        posicao: 'Avançado'
    };
    var j2 = {
        nome: "Pepe",
        dataNascimento: new Date("1969-2-25"),
        altura: "180",
        pais: "Brasil",
        posicao: "Defesa"
    };
    postJogador(j1);
    postJogador(j2);

    var e1 = {
        nome: "Sport Lisboa e Benfica",
        acronimo: 'SLB',
        pais: 'Portugal',
        url: "www.benfica.pt",
        descricao: 'o glorioso'
    };
    var e2 = {
        nome: 'Sporting Clube de Portugal',
        acronimo: 'SCP',
        pais: 'Portugal',
        url: 'www.sporting.pt',
        descricao: 'ninguém goza com o sporting'
    };
    var e3 = {
        nome: 'Futebol Clube do Porto',
        acronimo: 'FCP',
        pais: 'Portugal',
        url: 'www.fcporto.pt',
        descricao: 'Debes de ser burro para bires ler isto'
    };
    postEquipa(e1);
    postEquipa(e2);
    postEquipa(e3);

    postJogadorPlantel(1, 1);
    postJogadorPlantel(1, 2);

    var l1 = {
        nome: 'Liga NOS',
        edicao: '2016/17',
        nEquipas: 4
    };
    var l2 = {
        nome: 'Barcleys qualquer coisa',
        edicao: '2016/17',
        nEquipas: 12
    };
    var l3 = {
        nome: 'Liga BBVA',
        edicao: '2016/17',
        nEquipas: 20
    };

    postLiga(l1);
    postLiga(l2);
    postLiga(l3);

    var t1 = {
        nome: 'Eusébio Cup',
        edicao: '2017',
        nEquipas: 4
    };
    var t2 = {
        nome: 'Copa Emirates',
        edicao: '2017',
        nEquipas: 8
    };
    var t3 = {
        nome: 'Copa Bimbo',
        edicao: '2017',
        nEquipas: 2
    };

    postTaca(t1);
    postTaca(t2);
    postTaca(t3);
}

/**
 * @function getPaises
 * Devolve uma lista de países
 * @return {array} array de países
 */
function getPaises() {
    return paises;
}

/**
 * @function getPosicoes
 * Devolve uma lista de posições
 * @return {array} array de posições
 */
function getPosicoes() {
    return posicoes;
}

//Jogadores
/**
 * @function getJogadores
 * Devolve uma lista de jogadores
 * @return {array} array de jogadores
 */
function getJogadores() {
    return jogadores;
}

/**
 * @function getJogador
 * Devolve um jogador
 * @param  {number} id id do jogador é passado como parâmetro
 * @return {Jogador} um jogador é retornado
 */
function getJogador(id) {
    return jogadores.find(function (jogador) {
        return jogador.id == id;
    });
}

/**
 * @function deleteJogador
 * Apaga o jogador após encontrá-lo na lista de jogadores
 * @param  {number} id id do jogador a ser apagado
 */
function deleteJogador(id) {
    var jogador = getJogador(id);

    if (jogador.equipaId != -1) {
        deleteJogadorPlantel(jogador.equipaId, id);
    }

    var index = jogadores.indexOf(jogador);

    if (index > -1) {
        jogadores.splice(index, 1);
    }
}

/**
 * @function postJogador
 * Adiciona um jogador à lista de jogadores
 * @param  {Jogador} jogador Jogador a ser adicionado à lista
 */
function postJogador(jogador) {
    var j = new Jogador(
        jogador.nome,
        jogador.dataNascimento,
        jogador.altura,
        jogador.pais,
        jogador.posicao);
    jogadores.push(j);
}

/**
 * @function putJogador
 * Altera um jogador na lista de jogadores
 * @param  {Jogador} jogador jogador a ser alterado
 */
function putJogador(jogador) {
    var id = jogador.id;
    if (!getJogador(id)) {
        return undefined;
    }

    var j = getJogador(id);
    j.nome = jogador.nome;
    j.dataNascimento = jogador.dataNascimento;
    j.altura = jogador.altura;
    j.pais = jogador.pais;
    j.posicao = jogador.posicao;
}

//Equipas
/**
 * @function getEquipas
 * Devolve a lista de equipas
 * @return {array} retorna uma lista de equipas
 */
function getEquipas() {
    return equipas;
}

/**
* @function getEquipa
Devolve uma equipa da lista de equipas
* @param  {number} id é passado o id do da equipa como parâmetro
* @return {Jogador} retorna uma equipa da lista de equipas
*/
function getEquipa(id) {
    var equipas = getEquipas();
    return equipas.find(function (equipa) {
        return equipa.id == id;
    });
}

/**
 * @function postEquipa
 * Adiciona uma equipa à lista de equipas
 * @param  {Equipa} equipa Equipa a ser adicionada
 */
function postEquipa(equipa) {
    var e = new Equipa(
        equipa.nome,
        equipa.acronimo,
        equipa.pais,
        equipa.url,
        equipa.descricao
    );
    equipas.push(e);
}

/**
 * @function deleteEquipa
 * Remove uma equipa da lista de equipas
 * @param  {number} id id da equipa a ser apagada
 */
function deleteEquipa(id) {
    var equipa = getEquipa(id);
    var index = equipas.indexOf(equipa);

    var plantel = getPlantel(id);
    while (plantel.length > 0) {
        deleteJogadorPlantel(id, plantel[0]);
    }

    if (index > -1) {
        equipas.splice(index, 1);
    }
}

/**
 * @function putEquipa
 * Altera uma equipa
 * @param  {Equipa} equipa Equipa a ser alterada
 */
function putEquipa(equipa) {
    var id = equipa.id;
    if (!getEquipa(id)) {
        return undefined;
    }

    var e = getEquipa(id);
    e.nome = equipa.nome;
    e.acronimo = equipa.acronimo;
    e.pais = equipa.pais;
    e.url = equipa.url;
    e.descricao = equipa.descricao;
}

/**
 * @function getPlantel
 * @param  {number} id Id da equipa a mostrar o plantel
 * @return {array} Lista do plantel de jogadores da equipa
 */
function getPlantel(id) {
    return getEquipa(id).jogadores;
}

/**
 * @function postJogadorPlantel
 * Adiciona um jogador ao plantel
 * @param  {number} equipaId Id da equipa a que será adicionado o jogador
 * @param  {number} jogadorId Id do jogador que irá ser adicionado à equipa
 */
function postJogadorPlantel(equipaId, jogadorId) {
    getPlantel(equipaId).push(jogadorId);
    getJogador(jogadorId).equipaId = equipaId;
}

/**
 * @function deleteJogadorPlantel
 * Função para remover um jogador do plantel da equipa
 * @param  {number} equipaId  Id da equipa a que será adicionado o jogador
 * @param  {number} jogadorId Id do jogador que irá ser adicionado à equipa
 */
function deleteJogadorPlantel(equipaId, jogadorId) {
    var equipa = getEquipa(equipaId);
    var index = getPlantel(equipaId).indexOf(Number(jogadorId));
    getJogador(jogadorId).equipaId = -1;

    if (index > -1) {
        getPlantel(equipaId).splice(index, 1);
    }
}

//Ligas
/**
 * @function getLigas
 * Devolve a lista das equipas
 * @return {array} retorna uma lista de equipas
 */
function getLigas() {
    return ligas;
}

/**
 * @function getLiga
 * Devolve uma liga da lista de equipas
 * @param  {number} id Id da liga a ser devolvida
 * @return {Liga} retorna a liga da lista de equipas
 */
function getLiga(id) {
    var ligas = getLigas();
    return ligas.find(function (liga) {
        return liga.id == id;
    });
}

/**
 * @function postLiga
 * Função para adicionar ligas à lista
 * @param  {Liga} liga Liga a adicionar à lista de ligas
 */
function postLiga(liga) {
    var l = new Liga(
        liga.nome,
        liga.edicao,
        liga.nEquipas
    );
    ligas.push(l);
}

/**
 * @function deleteLiga
 * Função para remover liga da lista de ligas
 * @param  {number} id Id da liga a remover da lista
 */
function deleteLiga(id) {
    var liga = getLiga(id);
    var index = ligas.indexOf(liga);

    if (index > -1) {
        ligas.splice(index, 1);
    }
}

/**
 * @function putLiga
 * Função para atualizar os dados de uma liga
 * @param  {Liga} liga Liga a ser atualizada
 */
function putLiga(liga) {
    var id = liga.id;
    if (!getLiga(id)) {
        return undefined;
    }

    var l = getLiga(id);
    l.nome = liga.nome;
    l.edicao = liga.edicao;
    l.nEquipas = liga.nEquipas;
}

//Taças
/**
 * @function getTacas
 * Devolve uma lista de Taças
 * @return {array} retorna uma lista de Taças
 */
function getTacas() {
    return tacas;
}

/**
 * @function getTaca
 * Devolve uma taça da lista de taças
 * @return {Taça} retorna a taça da lista de taças
 */
function getTaca(id) {
    var tacas = getTacas();
    return tacas.find(function (taca) {
        return taca.id == id;
    });
}

/**
 * @function postTaca
 * Função que adiciona uma taça à lista de taças.
 * @param  {Taca} taca Taça a ser adicionada à lista
 */
function postTaca(taca) {
    var t = new Taca(
        taca.nome,
        taca.edicao,
        taca.nEquipas
    );
    tacas.push(t);
}

/**
 * @function deleteTaca
 * Função para remover uma taça da lista de taças
 * @param  {number} id Id da taça a reomover da lista
 */
function deleteTaca(id) {
    var taca = getTaca(id);
    var index = tacas.indexOf(taca);

    if (index > -1) {
        tacas.splice(index, 1);
    }
}

/**
 * @function putTaca
 * Função para actualizar valores da taça
 * @param  {Taca} taca Taca a ser inserida, para que os seus valores possam ser atualizados
 */
function putTaca(taca) {
    var id = taca.id;
    if (!getTaca(id)) {
        return undefined;
    }

    var t = getTaca(id);
    t.nome = taca.nome;
    t.edicao = taca.edicao;
    t.nEquipas = taca.nEquipas;
}

/**
 * Definições dos modelos
 */

var idJogador = 0;
/**
 * @function Jogador
 * @param  {string} nome           O Nome do jogador
 * @param  {Date} dataNascimento A data de nascimento do jogador
 * @param  {string} pais           O país do jogador
 * @param  {string} altura         A altura do jogador
 * @param  {string} posicao        A posicao do jogador
 * @return {Jogador} O jogador criado
 */
function Jogador(nome, dataNascimento, altura, pais, posicao) {
    this.id = ++idJogador;
    this.nome = nome;
    this.dataNascimento = dataNascimento;
    this.altura = altura;
    this.pais = pais;
    this.posicao = posicao;
    this.equipaId = -1;
}

var idEquipa = 0;
/**
 * @function Equipa
 * @param  {string} nome      O nome da equipa
 * @param  {string} acronimo  O acrónimo de 3 letras da equipa
 * @param  {string} pais      O país da equipa
 * @param  {string} url       O url para o website da equipa
 * @param  {string} descricao A descricao da equipa
 * @return {Equipa} A equipa criada
 */
function Equipa(nome, acronimo, pais, url, descricao) {
    this.id = ++idEquipa;
    this.nome = nome;
    this.acronimo = acronimo;
    this.pais = pais;
    this.url = url;
    this.descricao = descricao;
    this.jogadores = [];
}

var idCompeticao = 0;
/**
 * @function Competicao
 * @param  {string} nome     O nome da competição
 * @param  {string} edicao   A edição da competição
 * @return {Competicao} A competição criada
 */
function Competicao(nome, edicao, nEquipas) {
    this.id = ++idCompeticao;
    this.nome = nome;
    this.edicao = edicao;
    this.nEquipas = nEquipas;
    this.vencedor = undefined;
    this.equipas = [];
    this.jornadas = [];
}
Competicao.prototype.constructor = Competicao;

/**
 * @function Competicao.prototype.desinscreverEquipa
 * @param  {number} equipaId Id da equipa a desinscrever
 */
Competicao.prototype.desinscreverEquipa = function (equipaId) {
    if (!equipaId) return;
    if (!this.equipas.length) return;

    var index = this.equipas.findIndex(function (id) {
        return id == equipaId;
    });

    if (index > -1) {
        this.equipas.splice(index, 1);
    }
}

/**
 * @function Competicao.prototype.inscreverEquipa
 * @param  {number} equipaId Id da equipa a inscrever
 */
Competicao.prototype.inscreverEquipa = function (equipaId) {
    if (!equipaId) return;
    if (this.equipas.length == this.nEquipas) return;
    if (this.equipas.find(function (id) {
            return id == equipaId;
        })) return;

    this.equipas.push(equipaId);
}

/**
 * @function Competicao.prototype.estaInscrito
 * @param  {number} equipaId Id da equipa para verificar se está inscrito
 * @return {boolean} retorna true se estiver inscrito ou false caso contrário
 */
Competicao.prototype.estaInscrito = function (equipaId) {
    if (!equipaId) return;
    return this.equipas.find(function (id) {
        return id == equipaId;
    })
}

/**
 * @function Liga
 * @param  {string} nome   O Nome da liga
 * @param  {string} edicao A edição da liga
 * @return {Liga} A liga criada
 */
function Liga(nome, edicao, nequipas) {
    Competicao.apply(this, arguments);
}
Liga.prototype = Object.create(Competicao.prototype);
Liga.prototype.constructor = Competicao;

/**
 * @function Taca
 * @param  {string} nome     Nome da taça
 * @param  {string} edicao   Edição
 * @param  {number} nequipas Número de equipas
 * @return {Taca} A taca criada
 */
function Taca(nome, edicao, nEquipas) {
    Competicao.apply(this, arguments);
}
Taca.prototype = Object.create(Competicao.prototype);
Taca.prototype.constructor = Taca;

/**
 * Vista home 
 */

/**
* @function preencheListaCompeticoes
* Função para fazer uma lista não ordenada de competições
* @param  {HTMLElement} elementoHTML     Elemento HTML para que seja inseridos outros elementos dentro desse
* @param  {array} listaCompeticoes Lista de competições a serem preenchidas
*/
function preencheListaCompeticoes(elementoHTML, listaCompeticoes) {
    for (var i = 0; i < listaCompeticoes.length; i++) {
        var li = document.createElement('li');
        var nome = document.createTextNode(listaCompeticoes[i].nome);
        li.appendChild(nome);

        elementoHTML.appendChild(li);
    }
}

function competicoesADecorrer() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var competicoes = document.getElementById('competicoes-decorrendo');
    limpaElemento(competicoes);
    competicoes.parentNode.removeChild(competicoes);
    competicoes.style.display = 'block';

    var competicaoLiga = getLigas();
    var competicaoTaca = getTacas();

    if (!competicaoLiga) {
        return;
    }
    if (!competicaoTaca) {
        return;
    }

    var ligas = document.createElement('div');
    ligas.setAttribute('id', 'ligas-decorrendo');

    var textoLigas = document.createElement('h3');
    textoLigas.appendChild(document.createTextNode('Ligas a decorrer'));
    ligas.appendChild(textoLigas);

    var listaLigas = document.createElement('ul');
    preencheListaCompeticoes(listaLigas, competicaoLiga);
    ligas.appendChild(listaLigas);

    var tacas = document.createElement('div');
    tacas.setAttribute('id', 'tacas-decorrendo');

    var textoTacas = document.createElement('h3');
    textoTacas.appendChild(document.createTextNode('Taças a decorrer'));
    tacas.appendChild(textoTacas);

    var listaTacas = document.createElement('ul');
    preencheListaCompeticoes(listaTacas, competicaoTaca);
    tacas.appendChild(listaTacas);

    competicoes.appendChild(ligas);
    competicoes.appendChild(tacas);
    return competicoes;

}

/**
 * @function home
 * Função para apresentação da página inicial
 */
function home() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var home = document.getElementById('homes');
    home.parentNode.removeChild(home);
    home.style.display = 'block';

    document.body.appendChild(home);
    document.body.appendChild(competicoesADecorrer());
}

/**
 * Vista de Jogador
 */

/**
 * @function calcularIdade
 * @return {int} A idade do jogador
 */
function calcularIdade(data) {
    var dataAtual = new Date();

    anos = dataAtual.getUTCFullYear() - data.getFullYear();
    meses = dataAtual.getUTCMonth() - data.getMonth();
    dias = dataAtual.getUTCDate() - data.getDate();

    if (meses < 0 || meses === 0 && dias < 0) {
        return anos--;
    }

    return anos;
};

/**
 * @function removerJogadoresSelecionados
 * Devolve a tabela de jogadores sem o jogador que foi removido
 * @return {HTMLElement} Retorna o ecrã com a tabela de jogadores
 */
function removerJogadoresSelecionados() {
    var removido = false;
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            deleteJogador(linhas[i].getAttribute('id'));

            removido = true;
        }
    }

    return removido && menu('jogadores');
}

/**
 * @function getIdLinhaSelecionada
 * Devolve uma linha selecionada
 * @return {HTMLTableRowElement} retorna um o id da linha com o atributo 'selecionado'
 */
function getIdLinhaSelecionada() {
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            return linhas[i].getAttribute('id');
        }
    }

    return null;
}

var propsCabecalhoJogador = [
    'Nome',
    'Idade',
    'Altura (cm)',
    'País',
    'Posição',
    'Equipa'
];

/**
 * @function criarCabecalhoJogadores
 * Devolve um cabecalho de uma tabela de jogadores
 */
function criarCabecalhoJogadores() {
    return criarElementoCabecalho(propsCabecalhoJogador);
}

/**
 * @function criarLinhaJogador
 * Devolve uma linha com as informações de um jogador
 * @param  {Jogador} jogador O jogador
 * @return {HTMLElement} A linha criada
 */
function criarLinhaJogador(jogador) {
    var props = [];
    props.push(jogador.nome);
    props.push(calcularIdade(jogador.dataNascimento));
    props.push(jogador.altura);
    props.push(jogador.pais);
    props.push(jogador.posicao);
    props.push(jogador.equipaId === -1 ? '' : getEquipa(jogador.equipaId).acronimo);

    var tr = criarElementoLinhaTabela('td', props);
    tr.setAttribute('id', jogador.id);

    return tr;
}

/**
 * @function criarCorpoJogadores
 * Devolve o corpo de uma tabela (tbody) de jogadores
 * @return {HTMLElement} O corpo criado
 */
function criarCorpoJogadores(jogadores) {
    var tbody = document.createElement('tbody');

    for (var jogador in jogadores) {
        var tr = criarLinhaJogador(jogadores[jogador]);
        transformarLinhaClicavel(tr);
        tbody.appendChild(tr);
    }

    return tbody;
}

/**
 * @function criarTabelaJogadores
 * Devolve uma tabela de jogadores
 */
function criarTabelaJogadores(equipaId) {
    var textoCaption;
    switch (equipaId) {
        case -1:
        case undefined:
            textoCaption = 'Jogadores';
            break;
        default:
            textoCaption = 'Plantel de ' + getEquipa(equipaId).nome;
            break;
    }
    var table = criarElementoTabela(textoCaption);

    var thead = criarCabecalhoJogadores();
    table.appendChild(thead);

    var jogadores = [];
    if (equipaId && equipaId != -1) {
        var plantel = getPlantel(equipaId);
        for (var i = 0; i < plantel.length; i++) {
            jogadores.push(getJogador(plantel[i]));
        }
    } else {
        jogadores = getJogadores();
        if (equipaId == -1) {
            jogadores = jogadores.filter(function (j) {
                return j.equipaId == -1;
            });
        }
    }
    var tbody = criarCorpoJogadores(jogadores);

    table.appendChild(tbody);

    return table;
}

/**
 * @function mostrarFormularioJogador
 * Mostra o formulário do jogador.
 */
function mostrarFormularioJogador() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var formJogador = document.getElementById('formJogador');
    formJogador.parentNode.removeChild(formJogador);
    formJogador.style.display = 'block';


    document.body.appendChild(formJogador);
}

/**
 * @function preencheFormularioJogador
 * Preenche o formulário com os dados do jogador.
 * @param  {number} id Id do jogador que será usado para preencher o formulário
 */
function preencheFormularioJogador(id) {
    limpaFormularioJogador();
    var jogador = getJogador(id);

    var selectPais = document.getElementById('paisJogador');
    for (var i = 0; i < selectPais.childNodes.length; i++) {
        if (selectPais.children[i].getAttribute('value') === jogador.pais) {
            selectPais.children[i].setAttribute('selected', true);
        }
    }

    document.getElementById('idJogador').value = jogador.id;
    document.getElementById('nomeJogador').value = jogador.nome;
    document.getElementById('dNascimentoJogador').value = dateToDateFormatada(jogador.dataNascimento);
    document.getElementById('paisJogador').value = jogador.pais;
    document.getElementById('posicaoJogador').value = jogador.posicao;
    document.getElementById('alturaJogador').value = jogador.altura;
}

/**
 * @function limpaFormularioJogador
 * Limpa os valores que estão no formulário
 */
function limpaFormularioJogador() {
    document.getElementById('idJogador').value = '';
    document.getElementById('nomeJogador').value = '';
    document.getElementById('dNascimentoJogador').value = '';
    limpaElemento(document.getElementById('paisJogador'));
    criarOpcoesSelect('paisJogador', getPaises());
    limpaElemento(document.getElementById('posicaoJogador'));
    criarOpcoesSelect('posicaoJogador', getPosicoes());
    document.getElementById('alturaJogador').value = '';
}

/**
 * @function mostrarJogadores 
 * Página para mostrar o conteúdo referente aos jogadores.
 */
function mostrarJogadores() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    linhasSelecionadas = 0;

    var jogadores = document.createElement("div");
    jogadores.setAttribute('id', 'jogadores');
    var botoes = document.createElement("div");
    botoes.setAttribute('id', 'botoes');

    var tabela = criarTabelaJogadores();
    jogadores.appendChild(tabela);

    var trs = tabela.getElementsByTagName('tr');
    for (var i = 0; i < trs.length; i++) {
        if (trs[i].hasAttribute('id')) {
            adicionarEventoAtualizaBotaoEditar(trs[i]);
        }
    }

    var botaoCriar =
        criarElementoBotao(
            'Adicionar',
            'adicionarJogador',
            new EventoHTML(
                'click',
                limpaFormularioJogador
            ),
            new EventoHTML(
                'click',
                mostrarFormularioJogador
            )
        );
    var botaoRemover =
        criarElementoBotao(
            'Remover',
            'removerJogador',
            new EventoHTML(
                'click',
                removerJogadoresSelecionados
            )
        );
    var botaoEditar =
        criarElementoBotao(
            'Editar',
            'editarJogador',
            new EventoHTML(
                'click',
                function () {
                    preencheFormularioJogador(getIdLinhaSelecionada());
                }
            ), new EventoHTML(
                'click',
                mostrarFormularioJogador
            )
        );
    desativarBotao(botaoEditar);

    botoes.appendChild(botaoCriar);
    botoes.appendChild(botaoRemover);
    botoes.appendChild(botaoEditar);

    conteudo.appendChild(jogadores);
    conteudo.appendChild(botoes);
}

/**
 * @function submeterJogador
 * Submete o jogador para editar ou remover, verificando se tem um id ou não. Se não tiver, adiciona um novo jogador.
 */
function submeterJogador() {
    var jogador = {};
    jogador.id = document.getElementById("idJogador").value;
    jogador.nome = document.getElementById("nomeJogador").value;
    jogador.dataNascimento = new Date(Date.parse(document.getElementById('dNascimentoJogador').value));
    jogador.pais = document.getElementById("paisJogador").value;
    jogador.posicao = document.getElementById("posicaoJogador").value;
    jogador.altura = document.getElementById("alturaJogador").value;

    if (jogador.id) {
        putJogador(jogador);
    } else {
        postJogador(jogador);
    }

    menu('jogadores');
}

/**
 * Vista da equipa
 */

var propsCabecalhoEquipa = [
    'Nome',
    'Acrónimo',
    'País',
    'URL',
    'Nº de Jogadores',
];

/**
 * @function criarCabecalhoEquipas
 * Devolve uma função que cria o cabeçalho da tabela para a equipa
 * @return {HTMLElement} Retorna o cabeçalho da tabela da equipa
 */
function criarCabecalhoEquipas() {
    return criarElementoCabecalho(propsCabecalhoEquipa);
}

/**
 * @function criarLinhaEquipa
 * Devolve uma linha de uma tabela para uma equipa
 * @param  {Equipa} equipa A equipa a inserir na linha da tabela
 * @return {HTMLElement} A linha da tabela preenchida
 */
function criarLinhaEquipa(equipa) {
    var props = [];
    props.push(equipa.nome);
    props.push(equipa.acronimo);
    props.push(equipa.pais);
    props.push(equipa.url);
    props.push(equipa.jogadores.length);

    var tr = criarElementoLinhaTabela('td', props);
    tr.setAttribute('id', equipa.id);

    return tr;
}

/**
 * @function criarCorpoEquipas
 * Devolve um tbody da tabela de equipas
 * @return {HTMLTableSectionElement} retorna uma tbody da tabla de equipas
 */
function criarCorpoEquipas() {
    var tbody = document.createElement('tbody');
    var equipas = getEquipas();

    for (var equipa in equipas) {
        var tr = criarLinhaEquipa(equipas[equipa]);
        transformarLinhaClicavel(tr);
        tbody.appendChild(tr);
    }

    return tbody;
}

/**
 * @function criarTabelaEquipas
 * Devolve uma tabela de equipas
 * @return {HTMLElement} retorna uma tabela de equipas
 */
function criarTabelaEquipas() {
    var table = criarElementoTabela('Equipas');

    var thead = criarCabecalhoEquipas();
    table.appendChild(thead);

    var tbody = criarCorpoEquipas();
    table.appendChild(tbody);

    return table;
}

/**
 * @function limpaFormularioEquipa
 * Limpa o formulário da equipa
 */
function limpaFormularioEquipa() {
    document.getElementById('idEquipa').value = '';
    document.getElementById('nomeEquipa').value = '';
    document.getElementById('acronimoEquipa').value = '';
    limpaElemento(document.getElementById('paisEquipa'));
    criarOpcoesSelect('paisEquipa', getPaises());
    document.getElementById('urlEquipa').value = '';
    document.getElementById('descricaoEquipa').value = '';
}

/**
 * @function mostrarFormularioEquipa
 * Função para mostrar o formulário da equipa
 */
function mostrarFormularioEquipa() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var formEquipa = document.getElementById('formEquipa');
    formEquipa.parentNode.removeChild(formEquipa);
    formEquipa.style.display = 'block';

    document.body.appendChild(formEquipa);
}

/**
 * @function removerEquipasSelecionadas
 * Devolve a tabela de equipas sem a equipa selecionada
 * @return {HTMLElement} Volta para o ecrã que mostra a tabela das equipas
 */
function removerEquipasSelecionadas() {
    var removido = false;
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            deleteEquipa(linhas[i].getAttribute('id'));
            removido = true;
        }
    }

    return removido && menu('equipas');
}

/**
 * @function preencheFormularioEquipa
 * Preenche o formulário com os dados da equipa depois de receber o id da equipa
 * @param  {number} id id da equipa
 */
function preencheFormularioEquipa(id) {
    limpaFormularioEquipa();
    var equipa = getEquipa(id);

    var selectPais = document.getElementById('paisEquipa');
    for (var i = 0; i < selectPais.childNodes.length; i++) {
        if (selectPais.children[i].getAttribute('value') === equipa.pais) {
            selectPais.children[i].setAttribute('selected', true);
        }
    }

    document.getElementById('idEquipa').value = equipa.id;
    document.getElementById('nomeEquipa').value = equipa.nome;
    document.getElementById('acronimoEquipa').value = equipa.acronimo;
    document.getElementById('paisEquipa').value = equipa.pais;
    document.getElementById('urlEquipa').value = equipa.url;
    document.getElementById('descricaoEquipa').value = equipa.descricao;
}

/**
 * @function removerJogadoresPlantelSelecionados
 * Devolve o plantel sem o jogador que foi removido
 * @param  {number} equipaId Id da equipa
 * @return {HTMLElement} o plantel de jogadores sem o jogador removido da equipa
 */
function removerJogadoresPlantelSelecionados(equipaId) {
    var removido = false;
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            var j = getJogador(linhas[i].getAttribute('id'));
            deleteJogadorPlantel(equipaId, j.id);

            removido = true;
        }
    }

    return removido && mostrarPlantelEquipa(equipaId);
}

/**
 * @function adicionarJogadoresPlantelSelecionados
 * Função para adicionar os jogadores selecionados ao plantel da equipa
 * @param  {number} equipaId Id da equipa para adicionar jogador ao plantel
 * @return {boolean && function} retorna true se o jogador for adicionado ao plantel e retorna para a dunção que mostra o plantel da equipa, com o/os jogador/es adicionado/s
 */
function adicionarJogadoresPlantelSelecionados(equipaId) {
    var adicionado = false;
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            postJogadorPlantel(equipaId, linhas[i].getAttribute('id'));

            adicionado = true;
        }
    }

    return adicionado && mostrarPlantelEquipa(equipaId);
}

/**
 * @function mostrarJogadoresSemEquipa
 * Função que irá mostrar os jogadores sem equipa para que possam ser adicionados à equipa
 * @param  {number} equipaId Id da equipa
 */
function mostrarJogadoresSemEquipa(equipaId) {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    linhasSelecionadas = 0;

    var jogadores = document.createElement('div');
    jogadores.setAttribute('id', 'jogadores');
    var botoes = document.createElement('div');
    botoes.setAttribute('id', 'botoes');

    var tabela = criarTabelaJogadores(-1);
    jogadores.appendChild(tabela);

    var botaoAdicionar =
        criarElementoBotao(
            'Adicionar',
            'adicionarJogador',
            new EventoHTML(
                'click',
                function () {
                    adicionarJogadoresPlantelSelecionados(equipaId);
                }
            )
        );
    var botaoCancelar = criarElementoBotao(
        'Cancelar',
        'cancelarAdicionarJogador',
        new EventoHTML(
            'click',
            function () {
                mostrarPlantelEquipa(equipaId);
            }
        )
    );

    botoes.appendChild(botaoAdicionar);
    botoes.appendChild(botaoCancelar);

    conteudo.appendChild(jogadores);
    conteudo.appendChild(botoes);
}

/**
 * @function mostrarPlantelEquipa
 * Mostra o plantel da equipa selecionada
 * @param  {number} equipaId Id da equipa
 */
function mostrarPlantelEquipa(equipaId) {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var plantel = document.createElement('div');
    plantel.setAttribute('id', 'plantel');
    var botoes = document.createElement('div');
    botoes.setAttribute('id', 'botoes');

    var tabelaPlantel = criarTabelaJogadores(equipaId);
    plantel.appendChild(tabelaPlantel);

    var botaoAdicionar =
        criarElementoBotao(
            'Adicionar',
            'adicionarJogador',
            new EventoHTML(
                'click',
                function () {
                    mostrarJogadoresSemEquipa(equipaId);
                }
            )
        );
    var botaoRemover =
        criarElementoBotao(
            'Remover',
            'removerJogador',
            new EventoHTML(
                'click',
                function () {
                    removerJogadoresPlantelSelecionados(equipaId);
                }
            )
        );

    botoes.appendChild(botaoAdicionar);
    botoes.appendChild(botaoRemover);

    conteudo.appendChild(plantel);
    conteudo.appendChild(botoes);
}

/**
 * @function mostrarEquipas
 * Mostra todas as equipas numa tabela
 */
function mostrarEquipas() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    linhasSelecionadas = 0;

    var equipas = document.createElement('div');
    equipas.setAttribute('id', 'equipas');
    var botoes = document.createElement("div");
    botoes.setAttribute('id', 'botoes');

    var tabela = criarTabelaEquipas();
    equipas.appendChild(tabela);

    var trs = tabela.getElementsByTagName('tr');
    for (var i = 0; i < trs.length; i++) {
        if (trs[i].hasAttribute('id')) {
            adicionarEventoAtualizaBotaoEditar(trs[i]);
            adicionarEventoAtualizaBotaoPlantel(trs[i]);
        }
    }

    var botaoCriar =
        criarElementoBotao(
            'Adicionar',
            'adicionarEquipa',
            new EventoHTML(
                'click',
                limpaFormularioEquipa
            ),
            new EventoHTML(
                'click',
                mostrarFormularioEquipa
            )
        );
    var botaoRemover =
        criarElementoBotao(
            'Remover',
            'removerJogador',
            new EventoHTML(
                'click',
                removerEquipasSelecionadas
            )
        );
    var botaoEditar =
        criarElementoBotao(
            'Editar',
            'editarJogador',
            new EventoHTML(
                'click',
                function () {
                    preencheFormularioEquipa(getIdLinhaSelecionada());
                }
            ),
            new EventoHTML(
                'click',
                mostrarFormularioEquipa
            )
        );
    var botaoPlantel =
        criarElementoBotao(
            'Plantel',
            'plantelEquipa',
            new EventoHTML(
                'click',
                function () {
                    mostrarPlantelEquipa(getIdLinhaSelecionada());
                }
            )
        )

    desativarBotao(botaoEditar);
    desativarBotao(botaoPlantel);

    botoes.appendChild(botaoCriar);
    botoes.appendChild(botaoRemover);
    botoes.appendChild(botaoEditar);
    botoes.appendChild(botaoPlantel);

    conteudo.appendChild(equipas);
    conteudo.appendChild(botoes);
}

/**
 * @function submeterEquipa
 * Função para submeter equipa e editar se tiver um id ou adicionar se não tiver
 */
function submeterEquipa() {
    var equipa = {};
    equipa.id = document.getElementById('idEquipa').value;
    equipa.nome = document.getElementById('nomeEquipa').value;
    equipa.acronimo = document.getElementById('acronimoEquipa').value;
    equipa.pais = document.getElementById('paisEquipa').value;
    equipa.url = document.getElementById('urlEquipa').value;
    equipa.descricao = document.getElementById('descricaoEquipa').value;

    if (equipa.id) {
        putEquipa(equipa);
    } else {
        postEquipa(equipa);
    }

    menu('equipas');
}

/**
 * Vista da Competição
 */

var LIGA = 1;
var TACA = 2;

var propsCabecalhoCompeticao = [
    'Nome',
    'Edição',
    'Vencedor'
]

/**
 * @function criarCabecalhoCompeticoes
 * Função para devolver o cabeçalho das competições
 * @return {HTMLElement} retorna o cabeçalho da tabela
 */
function criarCabecalhoCompeticoes() {
    return criarElementoCabecalho(propsCabecalhoCompeticao);
}

/**
 * @function criarLinhaCompeticao
 * Função que devolve uma linha da competição
 * @param  {Competicao} competicao Competição a mostrar na linha
 * @return {HTMLElement} retorna a linha com os dados da competição
 */
function criarLinhaCompeticao(competicao) {
    var props = [];
    props.push(competicao.nome);
    props.push(competicao.edicao);
    var vencedor = 'SLB';
    props.push(vencedor);

    var tr = criarElementoLinhaTabela('td', props);
    tr.setAttribute('id', competicao.id);

    return tr;
}

/**
 * @function criarCorpoCompeticoes
 * Função que devolve um corpo de uma tabela conforme o tipo de competição
 * @param  {number} tipoCompeticao tipo de competição a criar o corpo
 * @return {HTMLElement} retorna um corpo de uma tabela
 */
function criarCorpoCompeticoes(tipoCompeticao) {
    var tbody = document.createElement('tbody');
    var competicoes;
    switch (tipoCompeticao) {
        case LIGA:
            competicoes = getLigas();
            break;
        case TACA:
            competicoes = getTacas();
            break;
        default:
            alert('erro muito estranho, contactar administrador');
    }

    for (var comp in competicoes) {
        var tr = criarLinhaCompeticao(competicoes[comp]);
        transformarLinhaClicavel(tr);
        tbody.appendChild(tr);
    }

    return tbody;
}

/**
 * @function criarTabelaCompeticoes
 * Função que devolve a tabela conforme o tipo de competição
 * @param  {number} tipoCompeticao tipo de competição a fazer a tabela
 * @return {HTMLElement} retorna uma tabela conforme o tipo de competição
 */
function criarTabelaCompeticoes(tipoCompeticao) {
    var stringTipoCompeticao = tipoCompeticao === 1 ? 'Ligas' : 'Taças';
    var table = criarElementoTabela(stringTipoCompeticao);

    var thead = criarCabecalhoCompeticoes();
    table.appendChild(thead);

    var tbody = criarCorpoCompeticoes(tipoCompeticao);
    table.appendChild(tbody);

    return table;
}

/**
 * @function removerCompeticoesSelecionadas
 * Função que remove a respetiva competição selecionada na tabela
 * @param  {number} tipoCompeticao Tipo de competição a ser removida
 * @return {boolean && function} retorna a função da respetiva competição, sem a competição que foi removida
 */
function removerCompeticoesSelecionadas(tipoCompeticao) {
    var removido = false;
    var linhas = document.getElementsByTagName('tr');
    for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].hasAttribute('selecionado')) {
            switch (tipoCompeticao) {
                case LIGA:
                    deleteLiga(linhas[i].getAttribute('id'));
                    break;
                case TACA:
                    deleteTaca(linhas[i].getAttribute('id'));
                    break;
            }
            removido = true;
        }
    }

    switch (tipoCompeticao) {
        case LIGA:
            return removido && menu('ligas');
        case TACA:
            return removido && menu('tacas');
    }
}

/**
 * @function preencheFormularioCompeticao
 * Função que irá preencher o formulário com os dados da competição
 * @param  {number} tipoCompeticao Tipo de competição irá ser usada para preencher o formulário
 * @param  {number} id             Id da competição
 */
function preencheFormularioCompeticao(tipoCompeticao, id) {
    limpaFormularioCompeticao(tipoCompeticao);
    var competicao;
    if (tipoCompeticao == LIGA)
        competicao = getLiga(id);
    if (tipoCompeticao == TACA)
        competicao = getTaca(id);

    document.getElementById('idCompeticao').value = competicao.id;
    document.getElementById('nomeCompeticao').value = competicao.nome;
    document.getElementById('edicaoCompeticao').value = competicao.edicao;
    document.getElementById('nEquipasCompeticao').value = competicao.nEquipas;
}

/**
 * @function getNumEquipasComp
 * Função que devolve o número de equipas numa dada competição
 * @param  {number} tipoCompeticao Tipo de competição para mostrar o número de equipas contidas
 * @return {array} retorna uma lista do número de equipas na competição
 */
function getNumEquipasComp(tipoCompeticao) {
    var numeros = [];
    for (var i = 4; i <= 20; i++) {
        numeros.push(i);
    }

    return numeros.filter(function (num) {
        if (tipoCompeticao == LIGA) {
            return num % 2 == 0;
        }
        if (tipoCompeticao == TACA) {
            return (num == 4 || num == 8 || num == 16);
        }
    })
}

/**
 * @function limpaFormularioCompeticao
 * Função que irá limpar o formulário de uma dada competição
 * @param  {number} tipoCompeticao Tipo de competição
 */
function limpaFormularioCompeticao(tipoCompeticao) {
    document.getElementById('idCompeticao').value = '';
    document.getElementById('tipoCompeticao').setAttribute('value', tipoCompeticao);
    document.getElementById('nomeCompeticao').value = '';
    document.getElementById('edicaoCompeticao').value = '';
    limpaElemento(document.getElementById('nEquipasCompeticao'));
    criarOpcoesSelect('nEquipasCompeticao', getNumEquipasComp(tipoCompeticao));
}

/**
 * @function mostrarFormularioCompeticao
 * Função para mostrar o formulário da respectiva competição
 * @param  {number} tipoCompeticao Tipo de competição a mostrar
 */
function mostrarFormularioCompeticao(tipoCompeticao) {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var formCompeticao = document.getElementById('formCompeticao');
    formCompeticao.parentNode.removeChild(formCompeticao);
    formCompeticao.style.display = 'block';

    document.body.appendChild(formCompeticao);
}

/**
 * @function preencheListaEquipas
 * Função para fazer uma lista não ordenada de equipas
 * @param  {HTMLElement} elementoHTML Elemento HTML para que seja inseridos outros elementos dentro desse
 * @param  {array} listaEquipas Lista de equipas a serem preenchidas
 */
function preencheListaEquipas(elementoHTML, listaEquipas) {
    for (equipa in listaEquipas) {
        var li = document.createElement('li');
        li.setAttribute('id', listaEquipas[equipa].id);

        li.appendChild(document.createTextNode(listaEquipas[equipa].nome));

        elementoHTML.appendChild(li);
    }
}

/**
 * @function mostrarPaginaInscrições
 * Função que irá mostrar uma página para inscrever equipas numa dada competição
 * @param  {number} tipoCompeticao Tipo de competição a mostrar na página
 * @param  {number} idCompeticao   Id da respectiva competição
 */
function mostrarPaginaInscrições(tipoCompeticao, idCompeticao) {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var competicao;
    switch (tipoCompeticao) {
        case LIGA:
            competicao = getLiga(idCompeticao);
            break;
        case TACA:
            competicao = getTaca(idCompeticao);
            break;
    }
    if (!competicao) {
        return;
    }

    var inscricoes = document.createElement('div');
    inscricoes.setAttribute('id', 'inscricoes');

    var divEquipasInscritas = document.createElement('div');
    divEquipasInscritas.setAttribute('id', 'equipasInscritas');

    var textoEquipasInscritas = document.createElement('h3');
    textoEquipasInscritas.appendChild(document.createTextNode('Equipas Inscritas ' + competicao.equipas.length + '/' + competicao.nEquipas));
    divEquipasInscritas.appendChild(textoEquipasInscritas);

    var equipasInscritas = document.createElement('ul');
    preencheListaEquipas(equipasInscritas, competicao.equipas.map(function (equipaId) {
        return getEquipa(equipaId);
    }));
    adicionarEventoDesinscrever(equipasInscritas, tipoCompeticao, idCompeticao);
    divEquipasInscritas.appendChild(equipasInscritas);

    var divEquipasNaoInscritas = document.createElement('div');
    divEquipasNaoInscritas.setAttribute('id', 'equipasNaoInscritas');

    var textoEquipasNaoInscritas = document.createElement('h3');
    textoEquipasNaoInscritas.appendChild(document.createTextNode('Equipas Não Inscritas'));
    divEquipasNaoInscritas.appendChild(textoEquipasNaoInscritas);

    var equipasNaoInscritas = document.createElement('ul');
    preencheListaEquipas(equipasNaoInscritas, getEquipas().filter(function (equipa) {
        return !competicao.estaInscrito(equipa.id);
    }));
    adicionarEventoInscrever(equipasNaoInscritas, tipoCompeticao, idCompeticao);
    divEquipasNaoInscritas.appendChild(equipasNaoInscritas);

    var botoes = document.createElement("div");
    botoes.setAttribute("id", "botoes");

    var botaoVoltar = criarElementoBotao(
        'Voltar', 'botaoVoltar',
        new EventoHTML(
            'click',
            function () {
                mostrarCompeticoes(tipoCompeticao);
            }
        )
    );

    var textoInstrucao = document.createElement('h1');
    textoInstrucao.appendChild(document.createTextNode('Clique numa das equipas para inscrever ou desinscrever'));

    inscricoes.appendChild(divEquipasInscritas);
    inscricoes.appendChild(divEquipasNaoInscritas);

    botoes.appendChild(botaoVoltar);

    conteudo.appendChild(textoInstrucao);
    conteudo.appendChild(inscricoes);
    conteudo.appendChild(botoes);
}

/**
 * @function mostrarCompeticoes
 * Função que irá mostrar uma dada competição
 * @param  {number} tipoCompeticao Tipo de competição a ser mostrada
 */
function mostrarCompeticoes(tipoCompeticao) {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    linhasSelecionadas = 0;

    var competicoes = document.createElement('div');
    competicoes.setAttribute('id', 'competicoes');
    var botoes = document.createElement('div');
    botoes.setAttribute('id', 'botoes');

    var tabela = criarTabelaCompeticoes(tipoCompeticao);
    competicoes.appendChild(tabela);

    var trs = tabela.getElementsByTagName('tr');
    for (var i = 0; i < trs.length; i++) {
        if (trs[i].hasAttribute('id')) {
            adicionarEventoAtualizaBotaoEditar(trs[i]);
            adicionarEventoAtualizaBotaoDetalhes(trs[i]);
            adicionarEventoAtualizaBotaoInscricoes(trs[i]);
        }
    }

    var botaoCriar =
        criarElementoBotao(
            'Adicionar',
            'adicionarCompeticao',
            new EventoHTML(
                'click',
                function () {
                    limpaFormularioCompeticao(tipoCompeticao);
                }
            ),
            new EventoHTML(
                'click',
                function () {
                    mostrarFormularioCompeticao(tipoCompeticao);
                }
            )
        );
    var botaoRemover =
        criarElementoBotao(
            'Remover',
            'removerCompeticao',
            new EventoHTML(
                'click',
                function () {
                    removerCompeticoesSelecionadas(tipoCompeticao);
                }
            )
        );
    var botaoEditar =
        criarElementoBotao(
            'Editar',
            'editarCompeticao',
            new EventoHTML(
                'click',
                function () {
                    preencheFormularioCompeticao(tipoCompeticao, getIdLinhaSelecionada());
                }
            ),
            new EventoHTML(
                'click',
                function () {
                    mostrarFormularioCompeticao(tipoCompeticao);
                }
            )
        );
    var botaoDetalhes =
        criarElementoBotao(
            'Detalhes',
            'detalhesCompeticao',
            new EventoHTML(
                'click',
                function () {
                    alert('ver detalhes da comp');
                }
            )
        );
    var botaoInscricoes =
        criarElementoBotao(
            'Inscrições',
            'inscricoesCompeticao',
            new EventoHTML(
                'click',
                function () {
                    mostrarPaginaInscrições(tipoCompeticao, getIdLinhaSelecionada());
                }
            )
        );
    desativarBotao(botaoEditar);
    desativarBotao(botaoDetalhes);
    desativarBotao(botaoInscricoes);

    botoes.appendChild(botaoCriar);
    botoes.appendChild(botaoRemover);
    botoes.appendChild(botaoEditar);
    botoes.appendChild(botaoDetalhes);
    botoes.appendChild(botaoInscricoes);

    conteudo.appendChild(competicoes);
    conteudo.appendChild(botoes);
}

/**
 * @function submeterCompeticao
 * Função para submeter um tipo de competição, seja ao adicionar alguma competição ou atualizar
 */
function submeterCompeticao() {
    var tipoCompeticao = document.getElementById('tipoCompeticao').value;
    var competicao = {};
    competicao.id = document.getElementById('idCompeticao').value;
    competicao.nome = document.getElementById('nomeCompeticao').value;
    competicao.edicao = document.getElementById('edicaoCompeticao').value;
    competicao.nEquipas = document.getElementById('nEquipasCompeticao').value;

    switch (Number(tipoCompeticao)) {
        case LIGA:
            if (competicao.id) {
                putLiga(competicao);
            } else {
                postLiga(competicao);
            }
            menu('ligas');
            break;
        case TACA:
            if (competicao.id) {
                putTaca(competicao);
            } else {
                postTaca(competicao);
            }
            menu('tacas');
            break;
    }
}

/**
 * Vista da Liga
 */

/**
 * @function mostrarLigas
 * Função que devolve a página das ligas
 * @return {function} retorna uma função com a página das ligas
 */
function mostrarLigas() {
    return mostrarCompeticoes(LIGA);
}

/**
 * Vista da Taça
 */

/**
 * @function mostrarTacas
 * Função que devolve a página das taças
 * @return {function} retorna uma função com a página das taças
 */
function mostrarTacas() {
    return mostrarCompeticoes(TACA);
}

/**
 * Vista do Sobre
 */

/**
 * @function mostrarSobre
 * Função que irá mostrar a página sobre os desenvolvedores
 */
function mostrarSobre() {
    var conteudo = getConteudo();
    limpaElemento(conteudo);

    var titulo = document.createElement("h1");

    var pProjeto = document.createElement("p");

    var divLinha = document.createElement("div");
    var divColuna1 = document.createElement("div");
    var divCartao1 = document.createElement("div");
    var imgD = document.createElement("img");
    var divContentor1 = document.createElement("div");
    var h2Dario = document.createElement("h2");
    var pEstudanteD = document.createElement("p");
    var pEmail = document.createElement("p");
    var aEmailD = document.createElement("a");
    var pButton1 = document.createElement("p");
    var button1 = document.createElement("button");

    var divColuna2 = document.createElement("div");
    var divCartao2 = document.createElement("div");
    var imgJ = document.createElement("iframe");
    var source = document.createElement('source');
    var divContentor2 = document.createElement("div");
    var h2Joao = document.createElement("h2");
    var pEstudanteJ = document.createElement("p");
    var pEmailJ = document.createElement("p");
    var aEmailJ = document.createElement("a");
    var pButton2 = document.createElement("p");
    var button2 = document.createElement("button");

    var divJSdoc = document.createElement("div");
    var pJSdoc = document.createElement("p");
    pJSdoc.appendChild(document.createTextNode("Pode ainda visualizar o JSdoc "));
    var aJSdoc = document.createElement("a");
    aJSdoc.setAttribute("href", "/scripts/out/index.html");
    aJSdoc.appendChild(document.createTextNode("aqui"));

    titulo.appendChild(document.createTextNode("Sobre nós"));

    pProjeto.appendChild(document.createTextNode("Este projeto foi desenvolvido no âmbito da disciplina de Programação para a Internet, com o objetivo da gestão realista de competições de um determinado desporto, tem de haver um principal foco na adequação dos conceitos do sistema ao próprio desporto que o sistema pretende suportar."));

    divLinha.setAttribute("class", "linha");

    /* Informação sobre o Dário */
    divCartao1.setAttribute("class", "cartao");
    divColuna1.setAttribute("class", "coluna");
    imgD.setAttribute("src", "/images/dario.png");
    imgD.setAttribute("alt", "Dário Botas");
    divContentor1.setAttribute("class", "contentor");
    h2Dario.appendChild(document.createTextNode("Dário Botas"));
    pEstudanteD.appendChild(document.createTextNode("Estudante de Engenharia Informática, no Ramo de Gestão, desenvolvedor do Sistema de Gestão de Competições de Futebol."));
    pEmail.appendChild(document.createTextNode("Email: "));
    aEmailD.setAttribute("href", "mailto:dario.botas@gmail.com");
    aEmailD.setAttribute("target", "_top");
    aEmailD.appendChild(document.createTextNode("dario.botas@gmail.com"));
    button1.onclick = function () {
        document.getElementById("estilo").href = "styles/estilos.css";
    };
    button1.appendChild(document.createTextNode("Aplicar Estilo CSS"));

    pButton1.appendChild(button1);
    pEmail.appendChild(aEmailD);
    divContentor1.appendChild(h2Dario);
    divContentor1.appendChild(pEstudanteD);
    divContentor1.appendChild(pEmail);
    divContentor1.appendChild(pButton1);
    divCartao1.appendChild(imgD);
    divCartao1.appendChild(divContentor1);
    divColuna1.appendChild(divCartao1);

    /* Informação sobre o João */
    divCartao2.setAttribute("class", "cartao");
    divColuna2.setAttribute("class", "coluna");
    
    imgJ.setAttribute('src', 'https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fjoaojuby%2Fvideos%2F1298890336814943%2F&show_text=0&width=400');
    imgJ.setAttribute("width", '282px');
    imgJ.setAttribute('height', '282px');
    divContentor2.setAttribute("class", "contentor");
    h2Joao.appendChild(document.createTextNode("João Silva"));
    pEstudanteJ.appendChild(document.createTextNode("Estudante de Engenharia Informática, no Ramo de Software, desenvolvedor do Sistema de Gestão de Competições de Futebol."));
    pEmailJ.appendChild(document.createTextNode("Email: "));
    aEmailJ.setAttribute("href", "mailto:@gmail.com");
    aEmailJ.setAttribute("target", "_top");
    aEmailJ.appendChild(document.createTextNode("@gmail.com"));
    button2.onclick = function () {
        document.getElementById("estilo").href = "styles/estilos.joao.css";
    };
    button2.appendChild(document.createTextNode("Aplicar Estilo CSS"));

    pButton2.appendChild(button2);
    pEmailJ.appendChild(aEmailJ);
    divContentor2.appendChild(h2Joao);
    divContentor2.appendChild(pEstudanteJ);
    divContentor2.appendChild(pEmailJ);
    divContentor2.appendChild(pButton2);
    divCartao2.appendChild(imgJ);
    divCartao2.appendChild(divContentor2);
    divColuna2.appendChild(divCartao2);

    divLinha.appendChild(divColuna1);
    divLinha.appendChild(divColuna2);

    pJSdoc.appendChild(aJSdoc);
    divJSdoc.appendChild(pJSdoc);

    conteudo.appendChild(titulo);
    conteudo.appendChild(pProjeto);
    conteudo.appendChild(divLinha);
    conteudo.appendChild(divJSdoc);
}

/**
 * Menu
 */

/**
 * @function adicionaEntradaMenu
 * Permite adicionar um elemento 'li' ao menu com um dado texto e id
 * @param  {HTMLUListElement} ul    O elemento ul do nav
 * @param  {string} texto O texto a mostrar no menu
 * @param  {string} id    O id a associar ao elemento
 */
function adicionaEntradaMenu(ul, texto, id) {
    var li = document.createElement('li');
    var a = document.createElement('a');
    var texto = document.createTextNode(texto);
    a.appendChild(texto);
    a.setAttribute('id', id);
    a.onclick = function () {
        menu(id);
    }
    li.appendChild(a);
    ul.appendChild(li);
}

/**
 * @function tornarMenuResponsivo
 * Adiciona um botão do tipo 'hamburguer' ao menu, para quando o ecrã
 * for pequeno para o menu normal
 * @param  {HTMLUListElement} ul O menu a tornar responsivo
 */
function tornarMenuResponsivo(ul) {
    var li = document.createElement('li');
    var a = document.createElement('a');
    var texto = document.createTextNode('☰');
    a.appendChild(texto);
    a.setAttribute('style', 'font-size:15px;');
    a.setAttribute('class', 'icon');
    a.onclick = function () {
        var nav = document.getElementsByTagName('nav')[0];
        if (nav.className === '')
            nav.className += 'responsivo';
        else
            nav.className = '';
    };
    li.appendChild(a);
    ul.appendChild(li);
}

/**
 * @function criarNav
 * Cria o elemento 'nav' na página
 */
function criarNav() {
    var body = document.getElementsByTagName('body')[0];
    var nav = document.createElement('nav');
    var ul = document.createElement('ul');
    var menu = [
        ['Home', 'home'],
        ['Jogadores', 'jogadores'],
        ['Equipas', 'equipas'],
        ['Ligas', 'ligas'],
        ['Taças', 'tacas'],
        ['Sobre', 'sobre']
    ];
    for (var i = 0; i < menu.length; i++) {
        adicionaEntradaMenu(ul, menu[i][0], menu[i][1]);
    }
    tornarMenuResponsivo(ul);
    nav.appendChild(ul);
    body.appendChild(nav);
}

var nomesForms = [
    'formJogador',
    'formEquipa',
    'formCompeticao'
];

/**
 * @function reorganizaBody
 * Função para reorganizar os elementos dentro do body
 */
function reorganizaBody() {
    var forms = [];
    for (var i = 0; i < nomesForms.length; i++) {
        forms.push(document.getElementById(nomesForms[i]));
    }
    while (forms.length > 0) {
        document.body.appendChild(forms.pop());
    }
}

/**
 * @function escondeForms
 * Função para esconder todos os formulários que estão no index.html
 */
function escondeForms() {
    for (form in nomesForms) {
        document.getElementById(nomesForms[form])
            .style.display = 'none';
    }
    document.getElementById('homes').style.display = 'none';
}

/**
 * @function adicionaLogicaForms
 * Função que irá fazer a lógica dos botões que estão nos formulários
 */
function adicionaLogicaForms() {
    var botaoCancelar, botaoSubmeter;

    //formJogador
    botaoCancelar = document.getElementById('formJogadorCancelar');
    botaoSubmeter = document.getElementById('formJogadorSubmeter');

    botaoCancelar.addEventListener('click', function () {
        menu('jogadores');
    });
    botaoSubmeter.addEventListener('click', submeterJogador);

    //formEquipa
    botaoCancelar = document.getElementById('formEquipaCancelar');
    botaoSubmeter = document.getElementById('formEquipaSubmeter');

    botaoCancelar.addEventListener('click', function () {
        menu('equipas');
    });
    botaoSubmeter.addEventListener('click', submeterEquipa);

    //formCompeticao
    botaoCancelar = document.getElementById('formCompeticaoCancelar');
    botaoSubmeter = document.getElementById('formCompeticaoSubmeter');

    botaoCancelar.addEventListener('click', menuCompeticao);
    botaoSubmeter.addEventListener('click', submeterCompeticao);
}

/**
 * @function menuCompeticao
 * Função para direcionar para as páginas das respetivas competições
 * @return {function} retorna a respetiva função que representa o menu da competição
 */
function menuCompeticao() {
    var tipoCompeticao = document.getElementById('tipoCompeticao').value;
    if (tipoCompeticao == LIGA) {
        return menu('ligas');
    }
    if (tipoCompeticao == TACA) {
        return menu('tacas');
    }
}

/**
 * @function menu
 * Permite alterar o conteúdo da página de acordo com a opção selecionada no menu
 * @param  {string} id O id da opção selecionada
 */
function menu(id) {
    if (!document.getElementsByTagName('nav')[0]) {
        criarNav();
        reorganizaBody();
    }

    escondeForms();

    var nav = document.getElementsByTagName('nav')[0];
    var ul = nav.childNodes.item(0);
    for (var i = 0; i < ul.childElementCount - 1; i++) {
        li = ul.childNodes.item(i);
        a = li.childNodes.item(0);
        if (a.getAttribute('id') === id)
            a.setAttribute('class', 'ativo');
        else
            (a.removeAttribute('class'));
    }

    switch (id) {
        case 'home':
            home();
            break;
        case 'jogadores':
            mostrarJogadores();
            document.getElementById('competicoes-decorrendo').style.display = 'none';
            break;
        case 'equipas':
            mostrarEquipas();
            document.getElementById('competicoes-decorrendo').style.display = 'none';
            break;
        case 'ligas':
            mostrarLigas();
            document.getElementById('competicoes-decorrendo').style.display = 'none';
            break;
        case 'tacas':
            mostrarTacas();
            document.getElementById('competicoes-decorrendo').style.display = 'none';
            break;
        case 'sobre':
            mostrarSobre();
            document.getElementById('competicoes-decorrendo').style.display = 'none';
            break;
    }
}

/**
 * Funções auxiliares
 */

/**
 * @function EventoHTML
 * @param  {string} evento   Tipo de evento a ser passado
 * @param  {function} callback função a ser passada como parâmetro
 */
var EventoHTML = function (evento, callback) {
    this.evento = evento;
    this.callback = callback;
}

/**
 * @function getConteudo
 * Devolve o elemento 'div' conteudo
 * Caso este elemento não exista, então é criado
 * @return {HTMLElement} O 'div' conteúdo
 */
function getConteudo() {
    var conteudo = document.getElementById('conteudo');
    if (!conteudo) {
        conteudo = document.createElement('div');
        conteudo.setAttribute('id', 'conteudo');
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(conteudo);
    }

    return conteudo;
}

/**
 * @function limpaElemento
 * @param  {HTMLElement} elementoHTML O elemento HTML a limpar
 */
function limpaElemento(elementoHTML) {
    while (elementoHTML.firstChild != undefined) {
        elementoHTML.removeChild(elementoHTML.firstChild);
    }
}

/**
 * @function criarElementoLinhaTabela
 * Devolve uma nova linha de uma tabela
 * @param  {string} tipo o tipo de linha, 'td' ou 'th'
 * @param  {Array} props Um array de strings com os valores a apresentar
 * @return {HTMLElement} A linha criada
 */
function criarElementoLinhaTabela(tipo, props) {
    var tr = document.createElement('tr');
    for (var i = 0; i < props.length; i++) {
        var celula = document.createElement(tipo);
        var texto = document.createTextNode(props[i]);
        celula.appendChild(texto);
        tr.appendChild(celula);
    }

    return tr;
}

/**
 * @function criarCabecalhoTabela
 * @param  {Array} props Array de propriedades para o cabeçalho da tabela.
 * @return {HTMLElement} Irá retornar um cabeçalho da tabela
 */
function criarElementoCabecalho(props) {
    var thead = document.createElement('thead');
    var th = criarElementoLinhaTabela('th', props);

    thead.appendChild(th);

    return thead;
}

/**
 * @function criarElementoTabela
 * Devolve uma tabela
 * @param  {string} textoCaption O texto a colocar na caption
 * @return {HTMLElement} A tabela criada
 */
function criarElementoTabela(textoCaption) {
    var table = document.createElement('table');

    if (textoCaption) {
        var caption = document.createElement('caption');
        var texto = document.createTextNode(textoCaption);
        caption.appendChild(texto);
        table.appendChild(caption);
    }

    return table;
}

/**
 * @function criarElementoBotao
 * Cria um botão, pode receber varios atributos
 * @param  {string} texto    O texto a mostrar no botão
 * @param  {string} id       O id do botão
 * @param  {string} evento   O tipo de evento a registar
 * @param  {function} callback A função a registar no evento
 * @return {HTMLInputElement} O botão criado
 */
function criarElementoBotao(texto, id) {
    var botao = document.createElement('input');

    botao.setAttribute('type', 'button');
    if (texto)
        botao.setAttribute('value', texto);
    if (id)
        botao.setAttribute('id', id);

    var eventos = Array.prototype.slice.call(arguments, 2);
    eventos.forEach(function (eventoHTML) {
        botao.addEventListener(eventoHTML.evento,
            eventoHTML.callback);
    });

    return botao;
}

/**
 * @function desativarBotao
 * Função que irá adicionar o atributo disable ao botão
 * @param  {HTMLElement} botao Botão a ser desativado
 */
function desativarBotao(botao) {
    botao.setAttribute('disabled', 'disabled');
}

var linhasSelecionadas = 0;

/**
 * @function eventoAtualizaBotaoInscricoes
 * Função para adicionar um evento no botão inscrições no caso de estar apenas uma linha selecionada, o botão ser habilitado
 * @param  {string} evt
 */
function eventoAtualizaBotaoInscricoes(evt) {
    var botaoInscricoes = document.getElementById('inscricoesCompeticao');

    if (linhasSelecionadas != 1) {
        desativarBotao(botaoInscricoes);
    } else {
        botaoInscricoes.removeAttribute('disabled');
    }
}

/**
 * @function eventoAtualizaBotaoDetalhes
 * Função para adicionar um evento no botão detalhes no caso de estar apenas uma linha selecionada, o botão ser habilitado
 * @param  {string} evt 
 */
function eventoAtualizaBotaoDetalhes(evt) {
    var botaoDetalhes = document.getElementById('detalhesCompeticao');

    if (linhasSelecionadas != 1) {
        desativarBotao(botaoDetalhes);
    } else {
        botaoDetalhes.removeAttribute('disabled');
    }
}

/**
 * @function eventoAtualizaBotaoEditar
 * Evento para habilitar botão editar caso esteja apenas uma linha selecionada
 * @param  {string} evt 
 */
function eventoAtualizaBotaoEditar(evt) {
    var botaoEditar = document.getElementById('editarJogador') ||
        document.getElementById('editarCompeticao');

    if (linhasSelecionadas != 1) {
        desativarBotao(botaoEditar);
    } else {
        botaoEditar.removeAttribute('disabled');
    }
}

/**
 * @function eventoAtualizaBotaoPlantel
 * Evento para habilitar botão plantel caso esteja apenas uma linha na tabela selecionada
 * @param  {string} evt 
 */
function eventoAtualizaBotaoPlantel(evt) {
    var botaoPlantel = document.getElementById('plantelEquipa');
    if (linhasSelecionadas != 1) {
        desativarBotao(botaoPlantel);
    } else {
        botaoPlantel.removeAttribute('disabled');
    }
}

/**
 * @function eventoLinhaClicavel
 * Função para mudar o estilo da linha, adicionando um atributo se não existir, ou removendo se for clicada
 * @param  {string} evt 
 */
function eventoLinhaClicavel(evt) {
    var elem = evt.target.parentNode;

    if (elem.hasAttribute('selecionado')) {
        elem.removeAttribute('selecionado');
        linhasSelecionadas--;

        return;
    }
    var attr = document.createAttribute('selecionado');
    evt.target.parentNode.setAttributeNode(attr);
    linhasSelecionadas++;
}

/**
 * @function adicionarEventoInscrever
 * Função que devolve a página das inscrições com a equipa clicada na lista dos inscritos
 * @param  {array} lista         lista das equpas
 * @param  {number} tipoCompeticao Tipo de competiçãp
 * @param  {number} idCompeticao   Id da competição
 * @return {function} retorna função que mostra a página das inscrições com a equipa clicada na lista dos inscritos
 */
function adicionarEventoInscrever(lista, tipoCompeticao, idCompeticao) {
    var competicao;
    switch (tipoCompeticao) {
        case LIGA:
            competicao = getLiga(idCompeticao);
            break;
        case TACA:
            competicao = getTaca(idCompeticao);
            break;
    }

    var elemLi = lista.getElementsByTagName('li');
    for (var i = 0; i < elemLi.length; i++) {
        elemLi[i].addEventListener('click', function () {
            competicao.inscreverEquipa(this.getAttribute('id'));
            return mostrarPaginaInscrições(tipoCompeticao, idCompeticao);
        });
    }
}

/**
 * @function adicionarEventoDesinscrever
 * Função que devolve a página das inscrições com a equipa clicada na lista dos não inscritos
 * @param  {array} lista         lista das equpas
 * @param  {number} tipoCompeticao Tipo de competiçãp
 * @param  {number} idCompeticao   Id da competição
 * @return {function} retorna função que mostra a página das inscrições com a equipa clicada na lista dos não inscritos
 */
function adicionarEventoDesinscrever(lista, tipoCompeticao, idCompeticao) {
    var competicao;
    switch (tipoCompeticao) {
        case LIGA:
            competicao = getLiga(idCompeticao);
            break;
        case TACA:
            competicao = getTaca(idCompeticao);
            break;
    }

    var elemLi = lista.getElementsByTagName('li');
    for (var i = 0; i < elemLi.length; i++) {
        elemLi[i].addEventListener('click', function () {
            competicao.desinscreverEquipa(this.getAttribute('id'));
            return mostrarPaginaInscrições(tipoCompeticao, idCompeticao);
        });
    }
}

/**
 * @function adicionarEventoAtualizaBotaoInscricoes
 * Função que irá adicionar um evento à linha selecionada que irá alterar o botão inscrições
 * @param  {HTMLElement} tr linha selecionada
 */
function adicionarEventoAtualizaBotaoInscricoes(tr) {
    tr.addEventListener('click', eventoAtualizaBotaoInscricoes);
}

/**
 * @function adicionarEventoAtualizaBotaoDetalhes
 * Função que irá adicionar um evento à linha selecionada que irá alterar o botão detalhes
 * @param  {HTMLElement} tr linha selecionada
 */
function adicionarEventoAtualizaBotaoDetalhes(tr) {
    tr.addEventListener('click', eventoAtualizaBotaoDetalhes);
}

/**
 * @function adicionarEventoAtualizaBotaoEditar
 * Função que irá adicionar um evento à linha selecionada que irá alterar o botão editar
 * @param  {HTMLElement} tr linha selecionada
 */
function adicionarEventoAtualizaBotaoEditar(tr) {
    tr.addEventListener('click', eventoAtualizaBotaoEditar);
}

/**
 * @function adicionarEventoAtualizaBotaoPlantel
 * Função que irá adicionar um evento à linha selecionada que irá alterar o botão plantel
 * @param  {HTMLElement} tr linha selecionada
 */
function adicionarEventoAtualizaBotaoPlantel(tr) {
    tr.addEventListener('click', eventoAtualizaBotaoPlantel);
}

/**
 * @function transformarLinhaClicavel
 * Função que irá adicionar um evento à linha selecionada para que o estilo dela possa ser alterado
 * @param  {HTMLElement} tr linha selecionada
 */
function transformarLinhaClicavel(tr) {
    tr.addEventListener("click", eventoLinhaClicavel);
}

/**
 * @function criarElementoComTexto
 * Devolve um elemento com um texto inserido nele
 * @param  {HTMLElement} tipoElemento Tipo de elemento a ser criado
 * @param  {string} texto        Texto a ser inserido
 * @return {HTMLElement} retorna o elemento com o texto inserido
 */
function criarElementoComTexto(tipoElemento, texto) {
    var elem = document.createElement(tipoElemento);
    elem.appendChild(document.createTextNode(texto));

    return elem;
}

/**
 * @function criarOpcoesSelect
 * @param  {string} selectId id do select no formulário
 * @param  {array} opcoes   array com as opções a serem inseridas no select
 * @return {HTMLElement} retorna um elemento do tipo select
 */
function criarOpcoesSelect(selectId, opcoes) {
    var select = document.getElementById(selectId);
    for (var i = 0; i < opcoes.length; i++) {
        var option = document.createElement("option");
        option.text = opcoes[i];
        option.value = opcoes[i];
        select.appendChild(option);
    }
    return select;
}

/**
 * @function dateToDateFormatada
 * Devolve a data no formato YYYY-MM-DD para ser exibida num <input type='date' />
 * @param  {Date} date a data a formatar
 * @return {string} a data formatada
 */
function dateToDateFormatada(date) {
    var d = date.toISOString().split("T")[0];

    return d;
}

/**
 * @function carregarEstilo
 * Carrega um ficheiro CSS para a página HTML
 * @param  {string} url      A localização do ficheiro CSS
 */
function carregarEstilo(url) {
    var head = document.getElementsByTagName('head')[0];
    var estilo = document.createElement('link');

    estilo.setAttribute('type', 'text/css');
    estilo.setAttribute('rel', 'stylesheet');
    estilo.setAttribute('href', url);
    estilo.setAttribute('id', 'estilo');
    head.appendChild(estilo);
}

window.onload = function () {
    carregarEstilo('/styles/estilos.css');
    iniciarBD();
    adicionaLogicaForms();
    menu('home');
}